require 'bundler'
Bundler.require
require 'sinatra/reloader'

# Feel free to modify this file as you want.

get '/' do
  @color = "grey"
  haml :index
end

get '/application.js' do
  coffee :application
end

get '/application.css' do
  sass :application
end
