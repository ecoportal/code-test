Instructions
============

Clone the following git repository to get started:

    git@bitbucket.org:ecoportal/code-test.git

Commit sensibly. You have just over an hour to attempt a total of 6 questions,
suggested times are given for each question. The first two questions must be
completed, the others can be attempted in any order, to any state of completion,
or skipped should you run out of time. Prioritise the ones that you think show
off your skills the most.

When you're done, generate an archive which includes `.git` and send it to us.


Question 1
----------

Without using `uniq`, define a function in ruby that accepts a list and returns
only the unique values in that list.

*2-5 minutes*

Question 2
----------

Implement a basic calculator in ruby that accepts expressions of the form
`"-1 + 3 - 2"`. Implement the following:

- Negation (`-1`)
- Addition (`1+1`)
- Subtraction (`2-1`)

eval, shelling out etc are not allowed.

*10 minutes*

Question 3
----------

Refactor/comment on the piece of "library" code found in `question_3.rb`. Include:

- Purpose
- Style
- Issues & how to fix, any suggested improvements

*10 minutes*

Question 4
----------

Produce a data model for the following. Use whatever format(s) you're comfortable
with (diagram, code, text explanation)

An organization contains many employees, which are broken up into various
subteams, which may be broken up further. An employee can be a member of more
than one subteam. Content may be permissioned against certain subteams or
directly against certain employees.

Briefly outline the strengths/weaknesses of your approach.

*10 minutes*

Question 5
----------

Create the following using HAML and the environment provided:

- 10 columns, variable height, all aligned so that their tops touch the top of the
  page. Each column has a heading.
- Each column contains a random (10 - 30) number of blocks which will scroll the
  inside of the column, but not the heading of the column. Blocks are full width
  of the column and stack vertically.
- Each block has one of the classes `blue` `red` `yellow` randomly applied and
  is styled accordingly.
- Use background colours to distinguish between the different elements/layers.

**If you're not comfortable with SASS** create and edit application.css in
public/

**If you're not comfortable with HAML** then it sucks to be you.. You're going
to need to modify the codebase to use ERB instead and change the view files to
match.

*20 minutes*

Question 6
----------

Add coffeescript and any UI desired to question 6 such that:

- You can filter to all `blue`, `red` or `yellow` blocks
- You can drag and drop blocks between lists
- You can add text to any block by clicking on it to trigger some sort of
  editor

You may use any javascript libraries you choose.

**If you're not comfortable with Coffeescript** create and edit application.js
in public/ instead.

*10 minutes*
