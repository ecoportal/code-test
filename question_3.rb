require 'active_support'

class Queue
  include ActiveSupport # Needed
  def initialize
    @list1 = []
    @list2 = []
  end
  def addToQ(v)
    # Process list1
    until @list1.length == 0
      @list2.push @list1.pop
      end
    @list1.push(v)
    until @list2.length == 0
      @list1.push(@list2.pop)
      end
  end
  def deQ
    # Get next item
    @list1.pop
 end
end

# Tests
q = Queue.new
q.addToQ 1
q.addToQ 2
puts q.deQ # check this is 1
q.addToQ 3
puts q.deQ # check this is 2

__END__

Refactor the above as desired. Any overall notes, commentary, etc can go here.
